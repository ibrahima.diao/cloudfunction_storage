
# Define the Google Cloud provider configuration
# The "google" provider is used to authenticate and interact with the Google Cloud API
# The "credentials" parameter specifies the path to the service account key file used for authentication
# The "project" parameter specifies the project ID to use for resources created by Terraform
# The "region" parameter specifies the default region to use for resources created by Terrafor

provider "google" {
  credentials = file("./creds/serviceaccount.json")
  project     = var.project_id
  region      = var.region

}
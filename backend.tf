# Define the backend configuration for storing Terraform state
# The "local" backend is used to store state on the local file system

terraform {
  backend "local" {}
}
# Generates an archive of the source code compressed as a .zip file.
#archive src_gcs
data "archive_file" "source_gcs" {
  type        = "zip"
  source_dir  = "${path.module}/src"
  output_path = "${path.module}/function_gcs.zip"
  depends_on = [
    random_string.c
  ]
}

resource "random_string" "c" {
  length  = 16
  special = false
}

# Add source code zip to the Cloud Function's bucket
resource "google_storage_bucket_object" "zip_gcs" {
  source       = data.archive_file.source_gcs.output_path
  content_type = "application/zip"

  # Append to the MD5 checksum of the files's content
  # to force the zip to be updated as soon as a change occurs
  name   = "src-${data.archive_file.source_gcs.output_md5}.zip"
  bucket = google_storage_bucket.function_bucket.name

  # Dependencies are automatically inferred so these lines can be deleted
  depends_on = [
    google_storage_bucket.function_bucket, # declared in `storage.tf`
    data.archive_file.source_gcs
  ]
}

# Define input variables for a Terraform module

variable "project_id" {
  default = "vote-for-innov"
}

variable "region" {
  default = "europe-west1"
}
variable "terraform_state_bucket_name" {
  default = "your-project-terraform-state"
}


